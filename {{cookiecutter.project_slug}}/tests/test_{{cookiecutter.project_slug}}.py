#!/usr/bin/env python

"""
test_{{ cookiecutter.project_slug }}
----------------------------------

Tests for `{{ cookiecutter.project_slug }}` module.
"""

from {{ cookiecutter.project_slug }} import {{ cookiecutter.project_slug }}


def test_{{ cookiecutter.project_slug}}() -> None:
    pass
