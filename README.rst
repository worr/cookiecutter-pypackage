======================
cookiecutter-pypackage
======================

Cookiecutter_ template for worr's Python packages.

* Based off of: https://github.com/audreyr/cookiecutter-pypackage/
* Free software: BSD license

Features
--------

* nose
* mypy
* flake8
* tox: 3.5

.. _Cookiecutter: https://github.com/audreyr/cookiecutter

Quickstart
----------

Generate a Python package project::

    cookiecutter https://gitlab.com/worr/cookiecutter-pypackage.git
